import publicIp from 'public-ip'

export const getClientIp = async () =>
  await publicIp.v4({
    fallbackUrls: ['https://ifconfig.co/ip'],
  })

export const createBasket = async () => {
  const customerId = 1
  const ipAddress = await getClientIp()
  const basket = null

  const url = `https://demo.storm.io/stormapi/1.1/ShoppingService.svc/rest/CreateBasket?ipAddress=${ipAddress}&createdBy=${customerId}&format=json`
  const options = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: basket,
  }

  const res = await fetch(url, options)
  const data = await res.json()
  return data
}

export const getBasket = async (basketId) => {
  const res = fetch(
    `https://demo.storm.io/stormapi/1.1/ShoppingService.svc/rest/GetBasket?format=json&id=${basketId}`
  )
  const data = await res.json()
  return data
}

export const addToBasket = async (product, basketId) => {
  const url = `https://demo.storm.io/stormapi/1.1/ShoppingService.svc/rest/InsertBasketItem?basketId=${basketId}format=json`
  const options = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: product,
  }

  const res = await fetch(url, options)
  const data = await res.json()
  return data
}
