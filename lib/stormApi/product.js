const baseUrl = 'https://demo.storm.io/stormapi/1.1/ProductService.svc/rest/'

export const getProducts = async (method, categoryId, size) => {
  try {
    let apiUrl = size
      ? `${baseUrl}${method}?format=json&categorySeed=${categoryId}&pageSize=${size}&pageNo=1`
      : `${baseUrl}${method}?format=json&categorySeed=${categoryId}`

    const res = await fetch(apiUrl)
    const data = await res.json()

    return data.Items || []
  } catch (error) {
    console.log('Error while fetching products:', error)
  }
}

export const getProductByUniqueName = async (uniqueName) => {
  try {
    const res = await fetch(
      baseUrl +
        'GetProductByUniqueName?uniqueName=' +
        uniqueName +
        '&format=json'
    )
    const data = await res.json()
    return data
  } catch (error) {
    console.log('Error while fetching product:', error)
  }
}
