const baseUrl = 'https://demo.storm.io/stormapi/1.1/ProductService.svc/rest/'

export const getBrands = async () => {
    try {
      const res = await fetch(baseUrl + 'ListManufacturers?format=json')
      const data = await res.json()
      return data.Items || []
    } catch (error) {
      console.log('Error while fetching brands:', error)
    }
  }
  