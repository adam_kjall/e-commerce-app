const baseUrl = 'https://demo.storm.io/stormapi/1.1/CustomerService.svc/rest/'

export const signUp = async (customer, password) => {
    const url = `${baseUrl}CreateCustomer3?password=${password}&doSendMail=false&format=json`
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(customer),
    }
  
    try {
      const res = await fetch(url, options)
      const data = await res.json()
      return data
    } catch (error) {
      console.log('Error creating customer:', error)
    }
  }
  
  export const login = async (email, password) => {
    try {
      const res = await fetch(
        `${baseUrl}Login?loginName=${email}&password=${password}&format=json`
      )
      const data = await res.json()
      return data
    } catch (error) {
      console.log('Error while login:', error)
    }
  }

  export const getUserByKey = async (key) => {
    try {
      const res = await fetch(
        `${baseUrl}GetCustomerByKey?key=${key}&format=json`
      )
      const data = await res.json()
      return data
    } catch (error) {
      console.log('Error while fetching user:', error)
    }
  }
  
  