const baseUrl = 'https://demo.storm.io/stormapi/1.1/ProductService.svc/rest/'

export const getCategories = async () => {
    try {
      const res = await fetch(baseUrl + 'ListCategories?format=json')
      const data = await res.json()
      // filter out testing categories
      const filteredCategories = data.filter(
        (cat) =>
          !cat.Value.trim().toLowerCase().includes('test') &&
          !cat.Value.trim().toLowerCase().includes('arkiv')
      )
      return filteredCategories || []
    } catch (error) {
      console.log('Error while fetching categories:', error)
    }
  }