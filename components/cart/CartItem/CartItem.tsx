import { useContext } from 'react'
import cn from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import { Trash, Plus, Minus } from '@components/icons'
import s from './CartItem.module.css'

import { CartContext } from 'contexts/cart/CartProvider'

const CartItem = ({
  product,
  currencyCode,
}: {
  product: any
  currencyCode: string
}) => {
  const {
    addProductToCart,
    removeProductFromCart,
    clearProductFromCart,
  } = useContext(CartContext)
  // const { price } = usePrice({
  //   amount: item.extended_sale_price,
  //   baseAmount: item.extended_list_price,
  //   currencyCode,
  // })
  // const updateItem = useUpdateItem(product)
  // const removeItem = useRemoveItem()
  // const [quantity, setQuantity] = useState(product.quantity)
  // const [removing, setRemoving] = useState(false)

  // const updateQuantity = async (val: number) => {
  //   await updateItem({ quantity: val })
  // }

  // const handleQuantity = (e: ChangeEvent<HTMLInputElement>) => {
  //   const val = Number(e.target.value)

  //   if (Number.isInteger(val) && val >= 0) {
  //     setQuantity(e.target.value)
  //   }
  // }
  // const handleBlur = () => {
  //   const val = Number(quantity)

  //   if (val !== product.quantity) {
  //     updateQuantity(val)
  //   }
  // }
  // const increaseQuantity = (n = 1) => {
  //   const val = Number(quantity) + n

  //   if (Number.isInteger(val) && val >= 0) {
  //     setQuantity(val)
  //     updateQuantity(val)
  //   }
  // }
  // const handleRemove = async () => {
  //   setRemoving(true)

  //   try {
  //     // If this action succeeds then there's no need to do `setRemoving(true)`
  //     // because the component will be removed from the view
  //     await removeItem({ id: product.id })
  //   } catch (error) {
  //     setRemoving(false)
  //   }
  // }

  // useEffect(() => {
  //   // Reset the quantity state if the item quantity changes
  //   if (item.quantity !== Number(quantity)) {
  //     setQuantity(item.quantity)
  //   }
  // }, [item.quantity])

  return (
    <li
      className={cn('flex flex-row space-x-8 py-8'
      // , {
      //   'opacity-75 pointer-events-none': removing,
      // }
      )}
    >
      <div className="w-16 h-16 bg-violet relative overflow-hidden">
        <Image
          className={s.productImage}
          src={`https://source.unsplash.com/${150}x${150}/?screens`} // TODO
          width={150}
          height={150}
          alt="Product Image"
          // The cart item image is already optimized and very small in size
          unoptimized
        />
      </div>
      <div className="flex-1 flex flex-col text-base">
        <Link href={`/product/${product.UniqueName}`}>
          <span className="font-bold mb-5 text-lg cursor-pointer">
            {product.Name}
          </span>
        </Link>

        <div className="flex items-center">
      
          <button className={`${product.quantity < 2 ? 'text-gray-300' : ''}`} type="button" onClick={() => removeProductFromCart(product)} disabled={product.quantity < 2}>
            <Minus width={18} height={18} />
          </button>
          <span className={s.quantity}>{product.quantity}</span>
          {/* <input
            type="number"
            max={99}
            min={0}
            className={s.quantity}
            value={product.quantity}
            onChange={handleQuantity}
            onBlur={handleBlur}
          /> */}
          <button type="button" onClick={() => addProductToCart(product)}>
            <Plus width={18} height={18} />
          </button>
        </div>
      </div>
      <div className="flex flex-col justify-between space-y-2 text-base">
        <span>{product.Price}</span>
        <button
          className="flex justify-end"
          onClick={() => clearProductFromCart(product)}
        >
          <Trash />
        </button>
      </div>
    </li>
  )
}

export default CartItem
