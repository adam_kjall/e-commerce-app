import { FC } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import { Github } from '@components/icons'
import { Logo, Container } from '@components/ui'
import { I18nWidget } from '@components/core'
import s from './Footer.module.css'

interface Props {
  className?: string
  children?: any
  pages?: any
}


const Footer: FC<Props> = ({ className, pages }) => {
  const rootClassName = cn(className)

  const sitePages = [
    {
      id: 7,
      channel_id: 1,
      name: 'About',
      meta_title: '',
      email: '',
      is_visible: true,
      parent_id: 0,
      sort_order: 0,
      description: null,
      type: 'page',
      meta_keywords: '',
      meta_description: '',
      is_homepage: false,
      is_customers_only: false,
      search_keywords: '',
      content_type: 'text/html',
      url: '/en-US/about/'
    }
  ]

  const legalPages = [
    {
      id: 6,
      channel_id: 1,
      name: 'Terms of use',
      meta_title: '',
      email: '',
      is_visible: true,
      parent_id: 0,
      sort_order: 1,
      description: null,
      type: 'page',
      meta_keywords: '',
      meta_description: '',
      is_homepage: false,
      is_customers_only: false,
      search_keywords: '',
      content_type: 'text/html',
      url: '/en-US/terms-of-use/'
    },
    {
      id: 2,
      channel_id: 1,
      name: 'Shipping & Returns',
      meta_title: '',
      email: '',
      is_visible: true,
      parent_id: 0,
      sort_order: 2,
      description: '',
      type: 'page',
      meta_keywords: '',
      meta_description: '',
      is_homepage: false,
      is_customers_only: false,
      search_keywords: '',
      content_type: 'text/html',
      url: '/en-US/shipping-returns/'
    },
    {
      id: 8,
      channel_id: 1,
      name: 'Privacy Policy',
      meta_title: '',
      email: '',
      is_visible: true,
      parent_id: 0,
      sort_order: 3,
      description: null,
      type: 'page',
      meta_keywords: '',
      meta_description: '',
      is_homepage: false,
      is_customers_only: false,
      search_keywords: '',
      content_type: 'text/html',
      url: '/en-US/privacy-policy/'
    }
  ]

  return (
    <footer className={rootClassName}>
      <Container>
        <div className="grid grid-cols-1 lg:grid-cols-12 gap-8 border-b border-accents-2 py-12 text-primary bg-primary transition-colors duration-150">
          <div className="col-span-1 lg:col-span-2">
            <Link href="/">
              <a className="flex flex-initial items-center font-bold md:mr-24">
                <span className="rounded-full border border-gray-700 mr-2">
                  <Logo />
                </span>
                <span>ACME</span>
              </a>
            </Link>
          </div>
          <div className="col-span-1 lg:col-span-2">
            <ul className="flex flex-initial flex-col md:flex-1">
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/">
                  <a className="text-primary hover:text-accents-6 transition ease-in-out duration-150">
                    Home
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/">
                  <a className="text-primary hover:text-accents-6 transition ease-in-out duration-150">
                    Careers
                  </a>
                </Link>
              </li>
              <li className="py-3 md:py-0 md:pb-4">
                <Link href="/blog">
                  <a className="text-primary hover:text-accents-6 transition ease-in-out duration-150">
                    Blog
                  </a>
                </Link>
              </li>
              {sitePages.map((page) => (
                <li key={page.url} className="py-3 md:py-0 md:pb-4">
                  <Link href={page.url!}>
                    <a className="text-primary hover:text-accents-6 transition ease-in-out duration-150">
                      {page.name}
                    </a>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
          <div className="col-span-1 lg:col-span-2">
            <ul className="flex flex-initial flex-col md:flex-1">
              {legalPages.map((page) => (
                <li key={page.url} className="py-3 md:py-0 md:pb-4">
                  <Link href={page.url!}>
                    <a className="text-primary hover:text-accents-6 transition ease-in-out duration-150">
                      {page.name}
                    </a>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
          <div className="col-span-1 lg:col-span-6 flex items-start lg:justify-end text-primary">
            <div className="flex space-x-6 items-center h-10">
              <a href="https://github.com/vercel/commerce" className={s.link}>
                <Github />
              </a>
              <I18nWidget />
            </div>
          </div>
        </div>
        <div className="py-12 flex flex-col md:flex-row justify-between items-center space-y-4">
          <div>
            <span>&copy; 2020 ACME, Inc. All rights reserved.</span>
          </div>
          <div className="flex items-center">
            <span className="text-primary">Crafted by</span>
            <a href="https://vercel.com" aria-label="Vercel.com Link">
              <img
                src="/vercel.svg"
                alt="Vercel.com Logo"
                className="inline-block h-6 ml-4 text-primary"
              />
            </a>
          </div>
        </div>
      </Container>
    </footer>
  )
}



export default Footer
