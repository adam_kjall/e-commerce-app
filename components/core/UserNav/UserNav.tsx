import { FC, useContext } from 'react'
import Link from 'next/link'
import cn from 'classnames'

import { Menu } from '@headlessui/react'
import { Heart, Bag } from '@components/icons'
import { Avatar } from '@components/core'
import { useUI } from '@components/ui/context'
import DropdownMenu from './DropdownMenu'
import s from './UserNav.module.css'

import { UserContext } from 'contexts/user/UserProvider'
import { CartContext } from 'contexts/cart/CartProvider'

interface Props {
  className?: string
}

const UserNav: FC<Props> = ({ className, children, ...props }) => {
  const { user } = useContext(UserContext)
  const { cart } = useContext(CartContext)
  const { toggleSidebar, closeSidebarIfPresent, openModal } = useUI()

  console.log("cart", cart);
  
  const itemsCount = cart.reduce(
    (count, product: any) => count + product.quantity,
    0
  )

  return (
    <nav className={cn(s.root, className)}>
      <div className={s.mainContainer}>
        <ul className={s.list}>
          <li className={s.item} onClick={toggleSidebar}>
            <Bag />
            {itemsCount > 0 && <span className={s.bagCount}>{itemsCount}</span>}
          </li>
          <li className={s.item}>
            <Link href="/wishlist">
              <a onClick={closeSidebarIfPresent}>
                <Heart />
              </a>
            </Link>
          </li>
          <li className={s.item}>
            {user ? (
              <Menu>
                {({ open }) => (
                  <>
                    <Menu.Button className={s.avatarButton} aria-label="Menu">
                      <Avatar />
                    </Menu.Button>
                    <DropdownMenu open={open} />
                  </>
                )}
              </Menu>
            ) : (
              <button
                className={s.avatarButton}
                aria-label="Menu"
                onClick={() => openModal()}
              >
                <Avatar />
              </button>
            )}
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default UserNav
