import { FC, useContext, useState } from 'react'
import cn from 'classnames'
import Image from 'next/image'
import { NextSeo } from 'next-seo'

import s from './ProductView.module.css'
import { useUI } from '@components/ui/context'
import { Swatch, ProductSlider } from '@components/product'
import { Button, Container } from '@components/ui'
import { HTMLContent } from '@components/core'

import {
  getCurrentVariant,
  getProductOptions,
  SelectedOptions,
} from '../helpers'
import WishlistButton from '@components/wishlist/WishlistButton'

import { CartContext } from 'contexts/cart/CartProvider'

interface Props {
  className?: string
  children?: any
  product: any
}

const ProductView: FC<Props> = ({ product }: any) => {
  const { addProductToCart } = useContext(CartContext)

  const { openSidebar } = useUI()
  const options = getProductOptions(product)
  const [loading, setLoading] = useState(false)
  const [choices, setChoices] = useState<SelectedOptions>({
    size: null,
    color: null,
  })
  // const variant =
  //   getCurrentVariant(product, choices)

  const addToCart = async () => {
    setLoading(true)
    try {
      // await addItem({
      //   productId: product.Id,
      //   variantId: 211
      //   // variantId: product.variants.edges?.[0]?.node.entityId!,
      // })
      addProductToCart(product)
      openSidebar()
      setLoading(false)
    } catch (err) {
      setLoading(false)
    }
  }

  return (
    <Container className="max-w-none w-full" clean>
      <NextSeo
        title={product.Name}
        description={product.Description}
        openGraph={{
          type: 'website',
          title: product.Name,
          description: product.Description,
          images: [
            {
              url: '/',
              width: 800,
              height: 600,
              alt: product.name,
            },
          ],
        }}
      />
      <div className={cn(s.root, 'fit')}>
        <div className={cn(s.productDisplay, 'fit')}>
          <div className={s.nameBox}>
            <h1 className={s.name}>{product.Name}</h1>
            <div className={s.price}>
              {product.Price}
              {` `}
              kr
              {/* TODO {product.prices?.price.currencyCode} */} 
            </div>
          </div>

          <div className={s.sliderContainer}>
            <Image
              className={s.img}
              src={`https://source.unsplash.com/${1050}x${1050}/?screens`} // TODO
              alt={'Product Image'}
              width={1050}
              height={1050}
              priority={true}
              quality="85"
            />
            {/* <ProductSlider>
              {product.images.edges?.map((image, i) => (
                <div key={image?.node.urlOriginal} className={s.imageContainer}>
                  <Image
                    className={s.img}
                    src={image?.node.urlOriginal!}
                    alt={image?.node.altText || 'Product Image'}
                    width={1050}
                    height={1050}
                    priority={i === 0}
                    quality="85"
                  />
                </div>
              ))}
            </ProductSlider> */}
          </div>
        </div>

        <div className={s.sidebar}>
          <section>
            {options?.map((opt: any) => (
              <div className="pb-4" key={opt.displayName}>
                <h2 className="uppercase font-medium">{opt.displayName}</h2>
                <div className="flex flex-row py-4">
                  {opt.values.map((v: any, i: number) => {
                    const active = (choices as any)[opt.displayName]

                    return (
                      <Swatch
                        key={`${v.entityId}-${i}`}
                        active={v.label === active}
                        variant={opt.displayName}
                        color={v.hexColors ? v.hexColors[0] : ''}
                        label={v.label}
                        onClick={() => {
                          setChoices((choices) => {
                            return {
                              ...choices,
                              [opt.displayName]: v.label,
                            }
                          })
                        }}
                      />
                    )
                  })}
                </div>
              </div>
            ))}

            <div className="pb-14 break-words w-full max-w-xl">
              <HTMLContent html={product.Description} />
            </div>
          </section>
          <div>
            <Button
              aria-label="Add to Cart"
              type="button"
              className={s.button}
              onClick={addToCart}
              loading={loading}
              // disabled={!variant}
            >
              Add to Cart
            </Button>
          </div>
        </div>

        <WishlistButton
          className={s.wishlistButton}
          productId={product.Id}
          // variant={product.variants.edges?.[0]!}
        />
      </div>
    </Container>
  )
}

export default ProductView
