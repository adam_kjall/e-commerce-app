import type { FC } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import { EnhancedImage } from '@components/core'
import s from './ProductCard.module.css'
import WishlistButton from '@components/wishlist/WishlistButton'

interface Props {
  className?: string
  product: any
  variant?: 'slim' | 'simple'
  imgWidth: number | string
  imgHeight: number | string
  priority?: boolean
}

const ProductCard: FC<Props> = ({
  className,
  product,
  variant,
  imgWidth,
  imgHeight,
  priority,
}) => {


  return (
    <Link href={`/product/${product.UniqueName}`}>
      <a
        className={cn(s.root, { [s.simple]: variant === 'simple' }, className)}
      >
        {variant === 'slim' ? (
          <div className="relative overflow-hidden box-border">
            <div className="absolute inset-0 flex items-center justify-end mr-8 z-20">
              <span className="bg-black text-white inline-block p-3 font-bold text-xl break-words">
                {product.Name}
              </span>
            </div>
            <EnhancedImage
              alt={product.Name}
              className={cn('w-full object-cover', s['product-image'])}
              src={`https://source.unsplash.com/${300}x${200}/?remotes`} // TODO fix image link
              width={imgWidth}
              height={imgHeight}
              priority={priority}
              quality="85"
            />
          </div>
        ) : (
          <>
            <div className={s.squareBg} />
            <div className="flex flex-row justify-between box-border w-full z-20 absolute">
              <div className="absolute top-0 left-0 pr-16 max-w-full">
                <h3 className={s.productTitle}>
                  <span>{product.Name}</span>
                </h3>
                <span className={s.productPrice}>{product.Price}</span>
              </div>
              <WishlistButton
                className={s.wishlistButton}
                productId={product.Id}
                // variant={product.variants.edges?.[0]!}
              />
            </div>
            <div className={s.imageContainer}>
              <EnhancedImage
                alt={product.Name}
                className={cn('w-full object-cover', s['product-image'])}
                src={`https://source.unsplash.com/${imgWidth}x${imgHeight}/?screens`} // TODO fix image link
                width={imgWidth}
                height={imgHeight}
                priority={priority}
                quality="85"
              />
            </div>
          </>
        )}
      </a>
    </Link>
  )
}

export default ProductCard
