

export type SelectedOptions = {
  size: string | null
  color: string | null
}

export type ProductOption = {
  displayName: string
  values: any
}

// Returns the available options of a product
export function getProductOptions(product: any) {
  // const options = product.productOptions.edges?.reduce<ProductOption[]>(
  //   (arr, edge) => {
  //     if (edge?.node.__typename === 'MultipleChoiceOption') {
  //       arr.push({
  //         displayName: edge.node.displayName.toLowerCase(),
  //         values: edge.node.values.edges?.map((edge) => edge?.node),
  //       })
  //     }
  //     return arr
  //   },
  //   []
  // )

  return []
}

// Finds a variant in the product that matches the selected options
export function getCurrentVariant(product: any, opts: SelectedOptions) {
  const variant = product.Variants.find((edge : any) => {
    const { node } = edge ?? {}

    return Object.entries(opts).every(([key, value]) =>
      node?.productOptions.edges?.find((edge : any) => {
        if (
          edge?.node.__typename === 'MultipleChoiceOption' &&
          edge.node.displayName.toLowerCase() === key
        ) {
          return edge.node.values.edges?.find(
            (valueEdge : any) => valueEdge?.node.label === value
          )
        }
      })
    )
  })

  return variant
}
