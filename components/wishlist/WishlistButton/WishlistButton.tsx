import React, { FC, useState, useContext } from 'react'
import cn from 'classnames'
import { Heart } from '@components/icons'
import { useUI } from '@components/ui/context'

import { UserContext } from "contexts/user/UserProvider"

type Props = {
  productId: number
  variant?: any
} & React.ButtonHTMLAttributes<HTMLButtonElement>

const WishlistButton: FC<Props> = ({
  productId,
  variant,
  className,
  ...props
}) => {
  const { user } = useContext(UserContext)
  // const addItem = useAddItem()
  // const removeItem = useRemoveItem()
  // const { data } = useWishlist()
  // const { data: customer } = useCustomer()
  const [loading, setLoading] = useState(false)
  const { openModal, setModalView } = useUI()
  // const itemInWishlist = data?.items?.find(
  //   (item) =>
  //     item.product_id === productId &&
  //     item.variant_id === variant?.node.entityId
  // )

  const handleWishlistChange = async (e: any) => {
    e.preventDefault()

    if (loading) return

    // A login is required before adding an item to the wishlist
    if (!user) {
      setModalView('LOGIN_VIEW')
      return openModal()
    }

    setLoading(true)

    // try {
    //   if (itemInWishlist) {
    //     await removeItem({ id: itemInWishlist.id! })
    //   } else {
    //     await addItem({
    //       productId,
    //       variantId: variant?.node.entityId!,
    //     })
    //   }

    //   setLoading(false)
    // } catch (err) {
    //   setLoading(false)
    // }
  }

  return (
    <button
      aria-label="Add to wishlist"
      className={cn({ 'opacity-50': loading }, className)}
      onClick={handleWishlistChange}
      {...props}
    >
      {/* TODO fill={true} only if item is on wishlist */}
      <Heart fill={false ? 'var(--pink)' : 'none'} /> 
    </button>
  )
}

export default WishlistButton
