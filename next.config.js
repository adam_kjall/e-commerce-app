module.exports = {
  images: {
    sizes: [320, 480, 820, 1200, 1600],
    domains: ['media.cdn.storm.io/2', 'source.unsplash.com'],
  },
  i18n: {
    locales: ['en-US', 'es'],
    defaultLocale: 'en-US',
  },
  rewrites() {
    return [
      {
        source: '/:locale/checkout',
        destination: '/api/checkout',
      },
      {
        source: '/checkout',
        destination: '/api/checkout',
      },
      // The logout is also an action so this route is not required, but it's also another way
      // you can allow a logout!
      {
        source: '/:locale/logout',
        destination: '/api/customers/logout?redirect_to=/',
      },
      {
        source: '/logout',
        destination: '/api/customers/logout?redirect_to=/',
      },
      // Rewrites for /search
      {
        source: '/:locale/search',
        destination: '/search',
      },
      {
        source: '/:locale/search/:path*',
        destination: '/search',
      },
      {
        source: '/search/designers/:name',
        destination: '/search',
      },
      {
        source: '/search/designers/:name/:category',
        destination: '/search',
      },
      {
        // This rewrite will also handle `/search/designers`
        source: '/search/:category',
        destination: '/search',
      },
    ]
  },
}
