import { Layout } from '@components/core'
import { Container, Text } from '@components/ui'

import { UserContext } from 'contexts/user/UserProvider'
import { useContext } from 'react'

export default function Profile() {
  const { user } = useContext(UserContext)

  return (
    <Container>
      <Text variant="pageHeading">My Profile</Text>
      {user && (
        <div className="grid lg:grid-cols-12">
          <div className="lg:col-span-8 pr-4">
            <div>
              <Text variant="sectionHeading">Name</Text>
              <span>
                {user.FirstName} {user.LastName}
              </span>
            </div>
            <div className="mt-5">
              <Text variant="sectionHeading">Email</Text>
              <span>{user.Email}</span>
            </div>
          </div>
        </div>
      )}
    </Container>
  )
}

Profile.Layout = Layout
