// import customersApi from '@bigcommerce/storefront-data-hooks/api/customers'

// export default customersApi()

import { NextApiRequest, NextApiResponse } from 'next'

import { getUserByKey } from '@lib/stormApi'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.query.key) {
    const user = await getUserByKey(req.query.key)
    res.json(user)
  } else {
    res.json({})
  }
}
