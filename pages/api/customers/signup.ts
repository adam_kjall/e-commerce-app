// import signupApi from '@bigcommerce/storefront-data-hooks/api/customers/signup'

// export default signupApi()
import { NextApiRequest, NextApiResponse } from 'next'

import { signUp } from '@lib/stormApi'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const customer = req.body
  const password = req.query.password

  const response = await signUp(customer, password)

  res.json(response)
}
