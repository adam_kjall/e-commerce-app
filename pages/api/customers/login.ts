//import loginApi from '@bigcommerce/storefront-data-hooks/api/customers/login'

//export default loginApi()

import { NextApiRequest, NextApiResponse } from 'next'

import { login } from '@lib/stormApi'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { email, password } = req.query
  const response = await login(email, password)

  res.json(response)
}
