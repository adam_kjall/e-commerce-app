import { NextApiRequest, NextApiResponse } from 'next'

import { getBasket } from 'lib/stormApi'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const basket = await getBasket(req.query.id)
    res.json(basket)
  } catch (error) {
    console.log('API Error creating basket', error)
    res.send(false)
  }
}
