import { NextApiRequest, NextApiResponse } from 'next'

import { createBasket } from "lib/stormApi"

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const basket = await createBasket()
    res.json(basket)

  } catch(error) {
    console.log("API Error creating basket", error)
    res.send(false)
  }
}
