import { useMemo } from 'react'
import type { GetStaticPropsContext, InferGetStaticPropsType } from 'next'

import rangeMap from '@lib/range-map'
import { Layout } from '@components/core'
import { Grid, Marquee, Hero } from '@components/ui'
import { ProductCard } from '@components/product'
import HomeAllProductsGrid from '@components/core/HomeAllProductsGrid'

import { getProducts, getCategories, getBrands} from "@lib/stormApi"

export async function getStaticProps({
  preview,
  locale,
}: GetStaticPropsContext) {
  const featuredProducts = await getProducts("ListPopularProducts2", 2192, 6)
  const bestSellingProducts = await getProducts("ListBestsellingProducts2", 2192,  6)
  const newestProducts = await getProducts("ListProducts2", 2192,  12)
  
  const categories = await getCategories();
  const brands = await getBrands();

  return {
    props: {
      featuredProducts,
      bestSellingProducts,
      newestProducts,
      categories,
      brands,
    },
    revalidate: 10,
  }
}

const nonNullable = (v: any) => v

export default function Home({
  featuredProducts,
  bestSellingProducts,
  newestProducts,
  categories,
  brands,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  // const { featured, bestSelling } = useMemo(() => {
  //   // Create a copy of products that we can mutate
  //   const products = [...newestProducts]
    
  //   // If the lists of featured and best selling products don't have enough
  //   // products, then fill them with products from the products list, this
  //   // is useful for new commerce sites that don't have a lot of products
  //   return {
  //     featured: rangeMap(6, (i) => featuredProducts[i] ?? products.shift())
  //       .filter(nonNullable)
  //       .sort((a, b) => a.node.prices.price.value - b.node.prices.price.value)
  //       .reverse(),
  //     bestSelling: rangeMap(
  //       6,
  //       (i) => bestSellingProducts[i] ?? products.shift()
  //     ).filter(nonNullable),
  //   }
  // }, [newestProducts, featuredProducts, bestSellingProducts])

  return (
    <div>
      <Grid>
        {/* featured */}
        {featuredProducts.slice(0, 3).map((product : any, i : number) => (
          <ProductCard
            key={product.Id}
            product={product}
            // The first image is the largest one in the grid
            imgWidth={i === 0 ? 1600 : 820}
            imgHeight={i === 0 ? 1600 : 820}
            priority
          />
        ))}
      </Grid>
      <Marquee variant="secondary">
        {/* best selling */}
        {bestSellingProducts.slice(3, 6).map((product : any) => (
          <ProductCard
            key={product.Id}
            product={product}
            variant="slim"
            imgWidth={320}
            imgHeight={320}
          />
        ))}
      </Marquee>
      <Hero
        headline="Release Details: The Yeezy BOOST 350 V2 ‘Natural'"
        description="
        The Yeezy BOOST 350 V2 lineup continues to grow. We recently had the
        ‘Carbon’ iteration, and now release details have been locked in for
        this ‘Natural’ joint. Revealed by Yeezy Mafia earlier this year, the
        shoe was originally called ‘Abez’, which translated to ‘Tin’ in
        Hebrew. It’s now undergone a name change, and will be referred to as
        ‘Natural’."
      />
      <Grid layout="B">
        {/* features */}
        {featuredProducts.slice(3, 6).map((product : any, i : number) => (
          <ProductCard
            key={product.Id}
            product={product}
            // The second image is the largest one in the grid
            imgWidth={i === 1 ? 1600 : 820}
            imgHeight={i === 1 ? 1600 : 820}
          />
        ))}
      </Grid>
      <Marquee>
        {/* best selling */}
        {bestSellingProducts.slice(0, 3).map((product : any) => (
          <ProductCard
            key={product.Id}
            product={product}
            variant="slim"
            imgWidth={320}
            imgHeight={320}
          />
        ))}
      </Marquee>
      <HomeAllProductsGrid
        categories={categories}
        brands={brands}
        newestProducts={newestProducts}
      />
    </div>
  )
}

Home.Layout = Layout
