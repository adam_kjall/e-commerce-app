import '@assets/main.css'
import 'keen-slider/keen-slider.min.css'

import { FC } from 'react'
import type { AppProps } from 'next/app'

import UserContextProvider from 'contexts/user/UserProvider'
import CartContextProvider from 'contexts/cart/CartProvider'

import { ManagedUIContext } from '@components/ui/context'
import { Head } from '@components/core'

const Noop: FC = ({ children }) => <>{children}</>

export default function MyApp({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout || Noop

  return (
    <>
      <Head />
      <ManagedUIContext>
        <UserContextProvider>
          <CartContextProvider>
            <Layout pageProps={pageProps}>
              <Component {...pageProps} />
            </Layout>
          </CartContextProvider>
        </UserContextProvider>
      </ManagedUIContext>
    </>
  )
}
