import { useReducer, createContext, useEffect, useState } from 'react'
import { cartReducer } from './reducers'
import { CartActions } from './actions'

export const CartContext = createContext({
  cart: [],
  metadata: {},
  addProductToCart: (product) => {},
  removeProductFromCart: (product) => {},
  clearProductFromCart: (product) => {},
  clearCart: () => {},
  calcTotalAmount: () => 0,
})

const CartProvider = (props) => {
  const [state, dispatch] = useReducer(cartReducer, { cart: [] })
  const [hasMounted, setHasMounted] = useState(false)

  // component did mount - fetch cart from local storage and save it in state
  useEffect(() => {
    const savedCart = JSON.parse(localStorage.getItem('cart'))
    if (savedCart) {
      dispatch({ type: CartActions.SET_CART, payload: savedCart })
    }
    setHasMounted(true)
    return () => setHasMounted(false)
  }, [])

  // on state.cart change update local storage
  useEffect(() => {
    if (!hasMounted) return
    localStorage.setItem('cart', JSON.stringify(state.cart))
  }, [state.cart])

  const addProductToCart = async (product) => {
    if (!state.metadata?.basketId) {
      const basket = await createBasket()

      console.log('basket', basket)
    }
    // dispatch({ type: CartActions.ADD_TO_CART, payload: product })
  }

  const removeProductFromCart = (product) =>
    dispatch({ type: CartActions.REMOVE_FROM_CART, payload: product })

  const clearProductFromCart = (product) =>
    dispatch({ type: CartActions.CLEAR_PRODUCT_FROM_CART, payload: product })

  const clearCart = () => dispatch({ type: CartActions.CLEAR_CART })

  const calcTotalAmount = () =>
    state.cart.reduce(
      (total, product) => total + product.Price * product.quantity,
      0
    )

  const createBasket = async () => {
    const res = await fetch(window.location.origin + '/api/basket/createBasket')
    return await res.json()
  }

  return (
    <CartContext.Provider
      value={{
        ...state,
        addProductToCart,
        removeProductFromCart,
        clearProductFromCart,
        clearCart,
        calcTotalAmount,
      }}
    >
      {props.children}
    </CartContext.Provider>
  )
}

export default CartProvider
