import { CartActions } from './actions'

export const cartReducer = (state, action) => {
  switch (action.type) {
    case CartActions.SET_CART:
      return {
        ...state,
        cart: action.payload,
      }
    case CartActions.ADD_TO_CART:
      return {
        ...state,
        cart: addProductToCart(state.cart, action.payload),
      }
    case CartActions.REMOVE_FROM_CART:
      return {
        ...state,
        cart: removeProductFromCart(state.cart, action.payload),
      }
    case CartActions.CLEAR_CART:
      return {
        ...state,
        cart: [],
      }
    case CartActions.CLEAR_PRODUCT_FROM_CART:
      return {
        ...state,
        cart: clearProductFromCart(state.cart, action.payload),
      }
    default:
      return state
  }
}

const addProductToCart = async (cart, productToAdd) => {
  const existing = cart.find((product) => product.Id === productToAdd.Id)

  if (existing) {
    return cart.map((product) => {
      if (product.Id === productToAdd.Id) {
        return { ...product, quantity: product.quantity + 1 }
      } else {
        return product
      }
    })
  }

  return [...cart, { ...productToAdd, quantity: 1 }]
}

const removeProductFromCart = (cart, productToRemove) => {
  if (productToRemove.quantity === 1) {
    return cart.filter((product) => product.Id !== productToRemove.Id)
  }

  return cart.map((product) => {
    if (product.Id === productToRemove.Id) {
      return {
        ...product,
        quantity: product.quantity - 1,
      }
    } else {
      return product
    }
  })
}

const clearProductFromCart = (cart, productToClear) => {
  return cart.filter((product) => product.Id !== productToClear.Id)
}
