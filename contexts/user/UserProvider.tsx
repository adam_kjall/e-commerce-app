import React, { createContext, FC, useState, useEffect } from 'react'

type User = {
  Account: {
    Authorizations: Array<any>
    Id: number
    IsActive: boolean
    Key: string
    LoginName: string
    Name: string
    Roles: Array<Number>
  }

  CellPhone: string | null
  Code: string
  Companies: Array<any>
  CrmId: number | null
  DeliveryAddresses: Array<any>
  Email: string
  FirstName: string
  Flags: Array<any>
  Id: number
  Info: Array<any>
  InvoiceAddress: any
  IsActive: boolean
  Key: string
  LastName: string
  Phone: string | null
  PricelistIds: Array<any>
  ReferId: any
  ReferUrl: any
  SSN: string | null
  UseInvoiceAddressAsDeliveryAddress: boolean
}

interface IUserContext {
  user: User | null
  signup: (customer: object, password: string) => Object
  login: (email: string, password: string) => Object
  logout: () => void
}

export const UserContext = createContext<IUserContext>({
  user: null,
  signup: () => ({}),
  login: () => ({}),
  logout: () => {},
})

const UserProvider: FC = (props) => {
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    const fetchUser = async (key : string) => {
      const res = await fetch(window.location.origin + "/api/customers?key=" + key)
      const data = await res.json()
      if (data) setUser(data as User)
    }

    const userKey = localStorage.getItem("user")
    if (userKey) {
      fetchUser(userKey)
    }
    
  }, [])

  const signup = async (customer: Object, password: String) => { 
    const response = await fetch(
      window.location.origin +
        '/api/customers/signup?password=' +
        password,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(customer),
      }
    )
    const data = await response.json()
      
    if (data.Message) return data

    setUser(data)
    return data
  }

  const login = async (email: String, password: String) => {
    const response = await fetch(
      window.location.origin +
        '/api/customers/login?email=' +
        email +
        '&password=' +
        password
    )
    const data = await response.json()
   
    if (data.Message) return data
    
    setUser(data) 
    localStorage.setItem("user", data.Key)
    return data
  }

  const logout = () => {
    setUser(null)
    localStorage.removeItem("user")
  }

  return (
    <UserContext.Provider {...props} value={{ user, signup, login, logout }} />
  )
}

export default UserProvider
